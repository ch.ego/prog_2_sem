
from itertools import combinations
import random
import networkx as nx
import matplotlib.pyplot as plt

from prog_2.lab3.main import find_shortest_path


def generate_random_graph(number_of_dots, probability_of_edge):
    V = set([v for v in range(number_of_dots)])
    E = set()
    for combination in combinations(V, 2):
        a = random.random()
        if a < probability_of_edge:
            E.add(combination)

    graph = nx.Graph()
    graph.add_nodes_from(V)
    graph.add_edges_from(E)

    return graph


operations_for_sizes = []
sizes = [n for n in range(10, 110, 10)]

for i in range(10, 110, 10):
    graph = generate_random_graph(i, 0.8)
    path, count = find_shortest_path(graph, list(graph.nodes())[0], list(graph.nodes())[-1])
    operations_for_sizes.append(count)

plt.plot(sizes, operations_for_sizes)
plt.xlabel('Размерность графа')
plt.ylabel('Количество операций')
plt.title('Сложность поиска кратчайшего пути в графе')
plt.show()
