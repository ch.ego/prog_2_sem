from prog_2.lab3.main import find_shortest_path


def test_correct_shortest_path():
    graph = {
        'A': ['B', 'E', 'C'],
        'B': ['A', 'D', 'E'],
        'C': ['A', 'F', 'G'],
        'D': ['B', 'E'],
        'E': ['A', 'B', 'D'],
        'F': ['C', 'I'],
        'G': ['C', 'H'],
        'H': ['G', 'I', 'J'],
        'I': ['H', 'J', 'F'],
        'J': ['H', 'I']
    }
    assert find_shortest_path(graph, 'A', 'J') == (['A', 'C', 'F', 'I', 'J'], 110)
