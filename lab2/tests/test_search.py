from prog_2.lab2.main import linear_search, binary_search, naive, KMP



def test_linear_search():
    assert linear_search([0, 1, 2, 3, 4, 5, 6, 7, 8, 9], 1) == 1
    assert linear_search([0, 1, 2, 3, 4, 5, 6, 7, 8, 9], 10) == -1


def test_binary_search():
    assert binary_search([0, 1, 2, 3, 4, 5, 6, 7, 8, 9], 1) == 1
    assert binary_search([0, 1, 2, 3, 4, 5, 6, 7, 8, 9], 10) == -1


def test_naive():
    assert naive('ПриветМир!', 'П') == 0
    assert naive('Привет Мир!', 'x') == -1


def test_kmp():
    assert KMP.partial('ПриветМир!', 'П') == 0
    assert KMP.partial('Привет Мир!', 'x') == -1
