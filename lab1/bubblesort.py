def sort_bubble(init_list):
    list_len = len(init_list)
    for i in range(list_len - 1):
        for j in range(list_len - i - 1):
            if init_list[j] > init_list[j + 1]:
                init_list[j], init_list[j + 1] = init_list[j + 1], init_list[j]
    return init_list
