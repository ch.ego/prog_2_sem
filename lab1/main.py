import time
from random import randint

from prog_2.lab1 import quicksort, bubblesort, insertionsort, shellsort


def main():
    input_list = []
    list_size = int(input('Введите длину списка для сортировки: '))
    for i in range(list_size):
        input_list.append(randint(0, 9999))

    print('Исходный список: ', input_list)
    sort(input_list)


def sort(alist):
    print("Быстрая сортировка")
    print(quicksort.sort_quick(alist), '\n')
    print("Пузырьковая сортировка")
    print(bubblesort.sort_bubble(alist), '\n')
    print("Сортировка вставками")
    print(insertionsort.sort_insertion(alist), '\n')
    print("Сортировка Шелла")
    print(shellsort.sort_shell(alist), '\n')


main()
