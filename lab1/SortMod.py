from enum import Enum


class SortMod(Enum):
    ASC = 1
    DESC = 2
