from enum import Enum


class SortType(Enum):
    BUBBLE = 1
    INSERTION = 2
    QUICK = 3
    SHELL = 4
