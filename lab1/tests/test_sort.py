import unittest

from prog_2.lab1.bubblesort import sort_bubble
from prog_2.lab1.insertionsort import sort_insertion
from prog_2.lab1.quicksort import sort_quick
from prog_2.lab1.shellsort import sort_shell


def test_bubble_correct():
    assert sort_bubble([1, 2, 5, 4, 7, 5, 8, 3, 5, 7]) == [1, 2, 3, 4, 5, 5, 5, 7, 7, 8]


def test_insertion_correct():
    assert sort_insertion([1, 2, 5, 4, 7, 5, 8, 3, 5, 7]) == [1, 2, 3, 4, 5, 5, 5, 7, 7, 8]


def test_quick_correct():
    assert sort_quick([1, 2, 5, 4, 7, 5, 8, 3, 5, 7]) == [1, 2, 3, 4, 5, 5, 5, 7, 7, 8]


def test_shell_correct():
    assert sort_shell([1, 2, 5, 4, 7, 5, 8, 3, 5, 7]) == [1, 2, 3, 4, 5, 5, 5, 7, 7, 8]


def test_sort_length():
    assert len(sort_bubble([1, 2, 5, 4, 7, 5, 8, 3, 5, 7])) == 10
    assert len(sort_insertion([1, 2, 5, 4, 7, 5, 8, 3, 5, 7])) == 10
    assert len(sort_quick([1, 2, 5, 4, 7, 5, 8, 3, 5, 7])) == 10
    assert len(sort_shell([1, 2, 5, 4, 7, 5, 8, 3, 5, 7])) == 10


def main():
    test_bubble_correct()
    test_insertion_correct()
    test_quick_correct()
    test_shell_correct()
    test_sort_length()


class MyTestCase(unittest.TestCase):
    pass


if __name__ == '__main__':
    unittest.main()
